<?php
namespace Tests\Evaneos\Kata\InvoiceReminder;

use Evaneos\Kata\InvoiceReminder\Notifier;
use Evaneos\Kata\InvoiceReminder\SoonDueReminder;
use Mockery;
use Mockery\Mock;
use Tests\Evaneos\Kata\InvoiceReminder\Builder\InvoiceBuilder;
use Tests\Evaneos\Kata\InvoiceReminder\Utils\FixedClock;

class SoonDueReminderTest extends \PHPUnit_Framework_TestCase
{
    /** @var Notifier|Mock */
    private $notifier;

    public function setUp()
    {
        $this->notifier = Mockery::mock(Notifier::class);
        $this->notifier->shouldReceive('notify');
    }

    /**
     * @test
     */
    public function it_notifies_10_days_before_due_date()
    {
        // Initialize
        $clock = new FixedClock(new \DateTimeImmutable('2017-01-23'));
        $invoice = (new InvoiceBuilder())
            ->withDueDate(new \DateTimeImmutable('2017-02-02'))
            ->build();

        // Act
        (new SoonDueReminder($this->notifier, $clock))
            ->remind($invoice);

        // Assert
        $this->notifier
            ->shouldHaveReceived('notify')
            ->with($invoice);
    }

    /**
     * @test
     */
    public function it_doesnt_notify_more_than_10_days_due_date()
    {
        // Initialize
        $clock = new FixedClock(new \DateTimeImmutable('2017-02-01'));
        $invoice = (new InvoiceBuilder())
            ->withDueDate(new \DateTimeImmutable('2017-02-15'))
            ->build();

        // Act
        (new SoonDueReminder($this->notifier, $clock))->remind($invoice);

        // Assert
        $this->notifier
            ->shouldNotHaveReceived('notify');
    }

    /**
     * @test
     */
    public function it_doesnt_notify_after_due_date()
    {
        // Initialize
        $clock = new FixedClock(new \DateTimeImmutable('2017-03-01'));
        $invoice = (new InvoiceBuilder())
            ->withDueDate(new \DateTimeImmutable('2017-02-15'))
            ->build();

        // Act
        (new SoonDueReminder($this->notifier, $clock))->remind($invoice);

        // Assert
        $this->notifier
            ->shouldNotHaveReceived('notify');
    }

    /**
     * @test
     */
    public function it_doesnt_notify_11_days_before_and_9_days_after_due_date()
    {
        // Initialize
        $beforeClock = new FixedClock(new \DateTimeImmutable('2017-02-04'));
        $afterClock = new FixedClock(new \DateTimeImmutable('2017-02-06'));

        $invoice = (new InvoiceBuilder())
            ->withDueDate(new \DateTimeImmutable('2017-02-15'))
            ->build();

        // Act
        (new SoonDueReminder($this->notifier, $beforeClock))->remind($invoice);
        (new SoonDueReminder($this->notifier, $afterClock))->remind($invoice);

        // Assert
        $this->notifier
            ->shouldNotHaveReceived('notify');
    }
}