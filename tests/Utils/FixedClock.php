<?php
namespace Tests\Evaneos\Kata\InvoiceReminder\Utils;

use Evaneos\Kata\InvoiceReminder\Clock\Clock;

class FixedClock implements Clock
{
    /** @var \DateTimeImmutable */
    private $now;

    /**
     * @param \DateTimeImmutable $now
     */
    public function __construct(\DateTimeImmutable $now)
    {
        $this->now = $now;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function now()
    {
        return $this->now;
    }
}