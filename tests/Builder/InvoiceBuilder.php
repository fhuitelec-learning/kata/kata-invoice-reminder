<?php
namespace Tests\Evaneos\Kata\InvoiceReminder\Builder;

use Evaneos\Kata\InvoiceReminder\Invoice;

class InvoiceBuilder
{
    /** @var \DateTimeImmutable */
    private $dueDate;

    public function __construct()
    {
        $this->dueDate = new \DateTimeImmutable();
    }

    /**
     * @param \DateTimeImmutable $dueDate
     * @return $this
     */
    public function withDueDate(\DateTimeImmutable $dueDate)
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * @return Invoice
     */
    public function build()
    {
        return new Invoice(
            $this->dueDate
        );
    }
}