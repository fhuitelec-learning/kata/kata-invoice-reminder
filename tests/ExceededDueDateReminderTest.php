<?php
namespace Tests\Evaneos\Kata\InvoiceReminder;

use Evaneos\Kata\InvoiceReminder\Notifier;
use Mockery\Mock;
use Tests\Evaneos\Kata\InvoiceReminder\Builder\InvoiceBuilder;
use Tests\Evaneos\Kata\InvoiceReminder\Utils\FixedClock;

class ExceededDueDateReminderTest extends \PHPUnit_Framework_TestCase
{
    /** @var Notifier|Mock */
    private $notifier;

    public function setUp()
    {
        $this->notifier = \Mockery::mock(Notifier::class);
    }

    /**
     * @test
     */
    public function it_notifies_when_invoice_has_not_been_paid()
    {
        $clock = new FixedClock(new \DateTimeImmutable('2017-02-01'));
        $invoice = (new InvoiceBuilder())
            ->withDueDate(new \DateTimeImmutable('2017-01-01'));

        (new ExceededDueDateReminder($this->notifier, $clock))
            ->remind($invoice);

        $this->notifier
            ->shouldHaveReceived('notify')
            ->with($invoice);
    }
}