<?php
namespace Evaneos\Kata\InvoiceReminder;

interface Notifier
{
    public function notify(Invoice $invoice);
}
