<?php
namespace Evaneos\Kata\InvoiceReminder\Clock;

interface Clock
{
    /**
     * @return \DateTimeImmutable
     */
    public function now();
}