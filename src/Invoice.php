<?php
namespace Evaneos\Kata\InvoiceReminder;

class Invoice
{
    /**
     * @var \DateTimeImmutable
     */
    private $dueDate;

    /**
     * Invoice constructor.
     * @param \DateTimeImmutable $dueDate
     */
    public function __construct(\DateTimeImmutable $dueDate)
    {
        $this->dueDate = $dueDate;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getDueDate() { return $this->dueDate; }

}
