<?php
namespace Evaneos\Kata\InvoiceReminder;

use Evaneos\Kata\InvoiceReminder\Clock\Clock;

class SoonDueReminder
{
    private $notifier;
    /** @var Clock */
    private $clock;

    public function __construct(Notifier $notifier, Clock $clock)
    {
        $this->notifier = $notifier;
        $this->clock = $clock;
    }

    public function remind(Invoice $invoice)
    {
        if ($this->isWithin10Days($invoice->getDueDate())) {
            $this->notifier->notify($invoice);
        }
    }

    public function isWithin10Days(\DateTimeImmutable $dueDate)
    {
        return $this->clock->now()->format('Y-m-d') === $dueDate->modify('-10 days')->format('Y-m-d');
    }
}
